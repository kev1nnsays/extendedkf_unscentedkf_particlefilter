%-------------------------------------------------------
% samples from a multivariate gaussian with given mean and covariance
%-------------------------------------------------------
function s = sample(mu, Sigma)

[V,D] = eig(Sigma);

% s=mu+V*sqrt(D)*randn(length(mu),1);
s=mu+V*sqrt(D)*randn(size(mu,1),size(mu,2));


function [newSamples, newWeight] = resample(samples, weight)
    numSamples = length(samples);
    newSamples = zeros(3,numSamples);
    weight = weight./sum(weight);
    c = cumsum(weight);
    u = unifrnd(0,1/numSamples);
    i = 1;
    
    for j = 1:numSamples
        while(u > c(i))
             i = i+1; 
        end
        newSamples(:,j) = samples(:,i);
        newWeight(:,j) = weight(:,i);
        u = u+1/numSamples;
        i =1;
    end
    newWeight = newWeight/sum(newWeight);
end
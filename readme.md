![Capture.PNG](https://bitbucket.org/repo/kEer6q/images/2082556091-Capture.PNG)

Matlab implementation of the Extended Kalman Filter, Unscented Kalman Filter, and Particle Filter

The function run.m generates motion information according to the odometry-based motion model. 
Observations are landmark detections sensed through noisy bearing measurements. Each landmark
has a unique ID and so data association is known for this exercise. Calling run(100) will generate a random
simulation of 100 time steps

### Implementation ###
* run.m – Main update loop, should call ekfUpdate(), ukfUpdate() and pfUpdate()  
* ekfUpdate.m – EKF update  
* ukfUpdate.m – UKF update  
* pfUpdate.m – PF update  
* resample.m – particle filter resampling, called by pfUpdate()  

### Utilities (you should not need to modify these files) ###
* getfieldinfo.m – gets field information  
* generateScript.m – generates data according to initial mean and noise parameters  
* generateMotion.m – simulates simple motion commands  
* sampleOdometry.m – implements Table 5.6  
* prediction.m – move robot according to specified motion  
* observation.m – returns an observation of the specified marker given the current state  
* endPoint.m – returns the location of an observation  
* sample.m – samples from a multivariate Gaussian  
* meanAndVariance.m – returns the mean and variance for a set of non-weighted samples (illustrates handling of angles)
* deg2rad.m – degrees to radians conversion  
* minimizedAngle.m – normalizes an angle to [−π,π]

### Display functions ###
* plotcircle.m – draws a circle  
* plotcov2d.m – draws a 2-D covariance matrix  
* plotfield.m – draws the field with landmarks  
* plotmarker.m – draws a dot at a specified 2D point (useful for plotting samples)  
* plotrobot.m – draws the robot  
* plotSamples.m – plots particles from the PF
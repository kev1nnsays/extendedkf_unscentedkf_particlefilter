function [ mu, Sigma, predMu, predSigma, zhat, G, R, H, K] = ukfUpdate( ...
    mu, Sigma, u, deltaT, M, z, Q, markerId)

% NOTE: The header is not set in stone.  You may change it if you like.
global FIELDINFO;
landmark_x = FIELDINFO.MARKER_X_POS(markerId);
landmark_y = FIELDINFO.MARKER_Y_POS(markerId);
z = z(1);
G = 1; R =1; H =1;
stateDim=3;
motionDim=3;
observationDim=2;

% --------------------------------------------
% Setup UKF
% --------------------------------------------

% UKF params
mu_aug = [mu' zeros(1,2) zeros(1,2)]'; %location, control, meas. means

% Augmented state
Sigma_aug = [Sigma, zeros(3), zeros(3,1);   %location cov
             zeros(3), M, zeros(3,1);       %control cov
             zeros(1,3) , zeros(1,3), Q];   %measurement cov
         
% Sigma points
n = 7; 
beta = 2; 
alpha = 1; 
k = 0;
lambda = alpha^2*(n+k)- n;
gamma = sqrt(lambda+n); 
Sigma_pts = [mu_aug, ...
            mu_aug * ones(1,7) + gamma.*sqrtm(Sigma_aug), ...
            mu_aug * ones(1,7) - gamma.*sqrtm(Sigma_aug)]; %7x15 Matrix
% Weights
w_m = 1/(2*(n+lambda)) .* ones(1,15);
w_c = 1/(2*(n+lambda)) .* ones(1,15);
w_m(1) = lambda/(n+lambda);
w_c(1) = lambda/(n+lambda) + (1-alpha^2+beta);

% --------------------------------------------
% Prediction step
% --------------------------------------------
Sigma_pts_state = Sigma_pts(1:3,:);
Sigma_pts_action = Sigma_pts(4:6,:);
uhat = u*ones(1,15) + Sigma_pts_action;
u_rot1 = uhat(1,:);
u_trans = uhat(2,:);
u_rot2 = uhat(3,:);
action = zeros(3,15);
for j = 1:15
    action(1,j) = u_trans(j)*cos(Sigma_pts_state(3,j)+u_rot1(j));
    action(2,j) = u_trans(j)*sin(Sigma_pts_state(3,j)+u_rot1(j));
    action(3,j) = u_rot1(j)+u_rot2(j);
    action(3,j) = checkAng(action(3,j));
end
pred_Sigma_pts_state = Sigma_pts_state  + action;

% UKF prediction of mean and covariance
predMu = zeros(3,1);
for j = 1:15
    predMu = predMu + w_m(j).* pred_Sigma_pts_state(:,j);
    predMu(3) = checkAng(predMu(3));
end
predSigma = zeros(3,3);
for j = 1:15
    A = pred_Sigma_pts_state(:,j) - predMu;
    A(3) = checkAng(A(3));
    B = pred_Sigma_pts_state(:,j) - predMu;
    B(3) = checkAng(B(3));
    predSigma = predSigma + ... 
                w_c(j).* A * B';
end

%--------------------------------------------------------------
% Correction step
%--------------------------------------------------------------
Zhat = zeros(1,15); 
for j = 1:15
    Zhat(j) = atan2(landmark_y - pred_Sigma_pts_state(2,j), landmark_x - pred_Sigma_pts_state(1,j)) -...
              pred_Sigma_pts_state(3,j) + Sigma_pts(7,j); 
%     Zhat(j) = checkAng(Zhat(j));
end
zhat = 0;
for j = 1:15
    zhat = zhat + w_m(j)*Zhat(j);
    zhat = checkAng(zhat); 
end
S = 0; 
for j = 1:15
    A = checkAng(Zhat(j)-zhat);
    S = S + w_c(j) .* A * A';
end
Sigma_XZ = 0; 
for j = 1:15
    A = pred_Sigma_pts_state(:,j)-predMu;
    A(3) = checkAng(A(3)); 
    B = checkAng(Zhat(j)-zhat);
    Sigma_XZ = Sigma_XZ + w_c(j)*A*B';
end
% UKF correction of mean and covariance
K = Sigma_XZ*S^-1;
mu = predMu + K*(z-zhat);
Sigma = predSigma - K*S*K';

end

function [angle] = checkAng(angle)
    if size(angle,1) ~= 1 &&  size(angle,2) ~= 1
        error('Angle is not scalar')
    end
    
    if angle > pi
        angle = angle - 2*pi;
    elseif angle < -pi
        angle = angle + 2*pi;
    else
        angle = angle; 
    end
end



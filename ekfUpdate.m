function [mu, Sigma, predMu, predSigma, zhat, G, R, H, K ] = ekfUpdate( ...
    mu, Sigma, u, deltaT, M, z, Q, markerId)

% NOTE: The header is not set in stone.  You may change it if you like.
global FIELDINFO;
landmark_x = FIELDINFO.MARKER_X_POS(markerId);
landmark_y = FIELDINFO.MARKER_Y_POS(markerId);

stateDim=3;
motionDim=3;
observationDim=2;

% --------------------------------------------
% Prediction step
% --------------------------------------------

% EKF prediction of mean and covariance

theta = mu(3);
rot1 = u(1); tran = u(2); rot2 = u(3);
G = [1, 0, -tran*sin(theta + rot1);
     0, 1, tran*cos(theta + rot1);
     0, 0, 1];
 
V = [-tran*sin(theta + rot1), cos(theta + rot1), 0; 
    tran*cos(theta+ rot1), sin(theta + rot1), 0; 
     1, 0, 1]; 
predMu = mu + [tran*cos(theta+rot1); tran*sin(theta+rot1); rot1+rot2];
predMu(3) = minimizedAngle(predMu(3));
predSigma = G*Sigma*G' + V*M*V';

%--------------------------------------------------------------
% Correction step
%--------------------------------------------------------------

% Compute expected observation and Jacobian
j = markerId; 
q = (landmark_x - predMu(1))^2 + (landmark_y - predMu(2))^2; 
zhat = atan2(landmark_y - predMu(2), landmark_x - predMu(1)) - predMu(3);
zhat = minimizedAngle(zhat);
    
H = [(landmark_y - predMu(2))/q, -(landmark_x - predMu(1))/q, -1; ];
% Innovation / residual covariance
S = H*predSigma*H' + Q;
% Kalman gain
K = predSigma * H' * inv(S);
% Correction
mu = predMu+K*(z(1) - zhat); 
mu = minimizedAngle(mu);
Sigma = (eye(3) - K*H)*predSigma; 
R = 1; 


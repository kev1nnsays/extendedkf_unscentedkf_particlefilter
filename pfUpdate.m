function [samples, weight, mu, Sigma, predStates, predMu, predSigma, zHat] = pfUpdate( ...
    samples, weight, numSamples, u, deltaT, M, z, Q, markerId, alphas)

% NOTE: The header is not set in stone.  You may change it if you like
global FIELDINFO;
landmark_x = FIELDINFO.MARKER_X_POS(markerId);
landmark_y = FIELDINFO.MARKER_Y_POS(markerId);

stateDim=3;
motionDim=3;
observationDim=2;
motion = u;
zVar = Q(1,1); 
zHat = 1;


% ----------------------------------------------------------------
% Prediction step
% ----------------------------------------------------------------

%Motion
%state = sampleOdometry(motion, state, alphas)
predStates = zeros(3,numSamples);
for j =1:numSamples
    predStates(:,j) = sampleOdometry(motion, samples(:,j), alphas);
end

% Compute mean and variance of estimate. Not really needed for inference.
[predMu, predSigma] = meanAndVariance(predStates, numSamples);

% ----------------------------------------------------------------
% Correction step
% ----------------------------------------------------------------
%Measurement weights
w = measurement_model(predStates,z, zVar, landmark_x, landmark_y); 
%Resample
[samples,nw] = resample(predStates,w);
[mu, Sigma] = meanAndVariance(samples, numSamples); %mean and cov of resamples
end

function [w] = measurement_model(states,z, zVar, landmark_x, landmark_y)
    w = zeros(1,length(states));
    for j = 1:length(states)
        zhat = atan2(landmark_y - states(2,j),landmark_x - states(1,j)) - states(3,j);
        zhat = minimizedAngle(zhat);
        w(j) = normpdf(z - zhat,0, sqrt(zVar)); 
    end
end


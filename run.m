function varargout = run(stepsOrData, pauseLen, makeVideo)
% RUN  PS2 Feature-Based Localization Simulator
%   RUN(ARG)
%   RUN(ARG, PAUSLEN, MAKEVIDEO)
%      ARG - is either the number of time steps, (e.g. 100 is a complete
%            circuit) or a data array from a previous run.
%      PAUSELEN - set to `inf`, to manually pause, o/w # of seconds to wait
%                 (e.g., 0.3 is the default)
%      MAKEVIDEO - boolean specifying whether to record a video or not
%
%   DATA = RUN(ARG,PAUSELEN)
%      DATA - is an optional output and contains the data array generated
%             and/or used during the simulation.

%   (c) 2009-2015
%   Ryan M. Eustice
%   University of Michigan
%   eustice@umich.edu

if ~exist('pauseLen','var') || isempty(pauseLen)
    pauseLen = 0.3; % seconds
end
if ~exist('makeVideo','var') || isempty(makeVideo)
    makeVideo = false;
end

%--------------------------------------------------------------
% Graphics
%--------------------------------------------------------------

NOISEFREE_PATH_COL = 'green';
ACTUAL_PATH_COL = 'blue';

NOISEFREE_BEARING_COLOR = 'cyan';
OBSERVED_BEARING_COLOR = 'red';

GLOBAL_FIGURE = 1;

if makeVideo
    try
        votype = 'avifile';
        vo = avifile('EKF.avi', 'fps', min(5, 1/pauseLen));
    catch
        votype = 'VideoWriter';
        vo = VideoWriter('video', 'MPEG-4');
        set(vo, 'FrameRate', min(5, 1/pauseLen));
        open(vo);
    end
end

%--------------------------------------------------------------
% Initializations
%--------------------------------------------------------------

initialStateMean = [180 50 0]';
%  initialStateMean =[rand(1)*500, rand(1)*300, -pi + rand(1)*2*pi]'; %kidnapping

% Motion noise (in odometry space, see Table 5.5, p.134 in book).
alphas = [0.05 0.001 0.05 0.01].^2; % variance of noise proportional to alphas

% Standard deviation of Gaussian sensor noise (independent of distance)
beta = deg2rad(20);

% Step size between filter updates, can be less than 1.
deltaT=0.1;

persistent data numSteps;
if isempty(stepsOrData) % use dataset from last time
    if isempty(data)
        numSteps = 100;
        data = generateScript(initialStateMean, numSteps, alphas, beta, deltaT);
    end
elseif isscalar(stepsOrData)
    % Generate a dataset of motion and sensor info consistent with
    % noise models.
    numSteps = stepsOrData;
    data = generateScript(initialStateMean, numSteps, alphas, beta, deltaT);
else
    % use a user supplied dataset from a previous run
    data = stepsOrData;
    numSteps = size(data, 1);
    global FIELDINFO;
    FIELDINFO = getfieldinfo;
end


% TODO: provide proper initialization for your filters here
% You can set the initial mean and variance of the EKF to the true mean and
% some uncertainty.

    method = 'EKF';
% Init Filters
    mu = initialStateMean ; 
%     mu = [180 50 0]' %kidnapped robot
    Sigma = eye(3);
    Q = [beta^2]; 
    
    %PF
    numSamples = 200;
    mu_sample = mu*ones(1,numSamples);
    samples = pfsample(mu_sample,Sigma);
    weight = 1;
    if strcmp(method, 'kidnapped')
        numSamples = 500;
        samples = [-100+rand(numSamples,1).*700,...
                    -100+ rand(numSamples,1).*500,...
                    -pi + rand(numSamples,1)*2*pi]';
    end
% Call ekfUpdate, ukfUpdate and pfUpdate in every iteration of this loop.
% You might consider putting in a switch yard so you can select which
% algorithm does the update
results = [];
for t = 1:numSteps
    
    %=================================================
    % data available to your filter at this time step
    %=================================================
    motionCommand = data(t,3:5)'; % [drot1, dtrans, drot2]' noisefree control command
    observation = data(t,1:2)';   % [bearing, landmark_id]' noisy observation
    %more reinitializations
    u = motionCommand; 
    z = observation; 


    %=================================================
    % data *not* available to your filter, i.e., known
    % only by the simulator, useful for making error plots
    %=================================================
    % actual position (i.e., ground truth)
    x = data(t,8);
    y = data(t,9);
    theta = data(t,10);

    % noisefree observation
    noisefreeBearing = data(t, 6);

    %=================================================
    % graphics
    %=================================================
    figure(GLOBAL_FIGURE); clf; hold on; plotfield(observation(2));

    % draw actual path and path that would result if there was no noise in
    % executing the motion command
    plot([initialStateMean(1) data(1,8)], [initialStateMean(2) data(1,9)], 'Color', ACTUAL_PATH_COL);
    plot([initialStateMean(1) data(1,11)], [initialStateMean(2) data(1,12)], 'Color', NOISEFREE_PATH_COL);

    % draw actual path (i.e., ground truth)
    plot(data(1:t,8), data(1:t,9), 'Color', ACTUAL_PATH_COL);
    plotrobot( x, y, theta, 'black', 1, 'cyan');

    % draw noise free motion command path
    plot(data(1:t,11), data(1:t,12), 'Color', NOISEFREE_PATH_COL);
    plot(data(t,11), data(t,12), '*', 'Color', NOISEFREE_PATH_COL);

    % indicate observed angle relative to actual position
    plot([x x+cos(theta+observation(1))*100], [y y+sin(theta+observation(1))*100], 'Color', OBSERVED_BEARING_COLOR);

    % indicate ideal noise-free angle relative to actual position
    plot([x x+cos(theta+noisefreeBearing)*100], [y y+sin(theta+noisefreeBearing)*100], 'Color', NOISEFREE_BEARING_COLOR);

    %=================================================
    %TODO: update your filter here based upon the
    %      motionCommand and observation
    %=================================================
    
    switch(method)
        case {'EKF'}
            M = [alphas(1)*u(1)^2 + alphas(2)*u(2)^2, 0, 0;
                0, alphas(3)*u(2)^2 + alphas(4)*u(1)^2+alphas(4)*u(3)^2, 0;
                0, 0, alphas(1)*u(3)^2 + alphas(2)*u(2)^2];
            markerId = z(2); 
            [mu, Sigma, predMu, predSigma, zhat, G, R, H, K ] = ekfUpdate( ...
            mu, Sigma, u, deltaT, M, z, Q, markerId);
        case {'UKF'}
            M = [alphas(1)*u(1)^2 + alphas(2)*u(2)^2, 0, 0;
                0, alphas(3)*u(2)^2 + alphas(4)*u(1)^2+alphas(4)*u(3)^2, 0;
                0, 0, alphas(1)*u(3)^2 + alphas(2)*u(2)^2];
            markerId = z(2); 
            [mu, Sigma, predMu, predSigma, zhat, G, R, H, K ] = ukfUpdate( ...
            mu, Sigma, u, deltaT, M, z, Q, markerId);
        case {'PF'} 
            M = [alphas(1)*u(1)^2 + alphas(2)*u(2)^2, 0, 0;
                0, alphas(3)*u(2)^2 + alphas(4)*u(1)^2+alphas(4)*u(3)^2, 0;
                0, 0, alphas(1)*u(3)^2 + alphas(2)*u(2)^2];
            markerId = z(2);
            z = z(1);
            [samples, weight, mu, Sigma, predState, predMu, predSigma, zHat] = pfUpdate( ...
            samples, weight, numSamples, u, deltaT, M, z, Q, markerId, alphas);
        case{'kidnapped'}
             M = [alphas(1)*u(1)^2 + alphas(2)*u(2)^2, 0, 0;
                0, alphas(3)*u(2)^2 + alphas(4)*u(1)^2+alphas(4)*u(3)^2, 0;
                0, 0, alphas(1)*u(3)^2 + alphas(2)*u(2)^2];
            markerId = z(2);
            z = z(1);
            [samples, weight, mu, Sigma, predState, predMu, predSigma, zHat] = pfUpdate( ...
            samples, weight, numSamples, u, deltaT, M, z, Q, markerId, alphas);
        
%             alphas2 = alphas*2;
%             M = [alphas2(1)*u(1)^2 + alphas2(2)*u(2)^2, 0, 0;
%                 0, alphas2(3)*u(2)^2 + alphas2(4)*u(1)^2+alphas2(4)*u(3)^2, 0;
%                 0, 0, alphas2(1)*u(3)^2 + alphas2(2)*u(2)^2];
%             
%             [mu, Sigma, predMu, predSigma, zhat, G, R, H, K ] = ukfUpdate( ...
%             mu, Sigma, u, deltaT, M, z, Q, markerId);
    end
    %=================================================
    %TODO: plot and evaluate filter results here
    %=================================================
    switch(method)
        case{'EKF'}
            plotmarker([predMu(1), predMu(2)], 'c');
            pred_sigma_graph= plotcov2d(predMu(1), predMu(2), predSigma, 'c', 0, 'y', 0.5, 3);%centerX, centerY, cov, color, filled, fillColor, falpha, nSigma

            plotmarker([mu(1), mu(2)], 'm');
            corr_sigma_graph=plotcov2d(mu(1), mu(2), Sigma, 'm', 0, 'y', 0.5, 3);%centerX, centerY, cov, color, filled, fillColor, falpha, nSigma
            legend([pred_sigma_graph,corr_sigma_graph],{'Pred EKF','Corrected EKF'});
            title('EKF Robot Localization')
        case{'UKF'}
            plotmarker([predMu(1), predMu(2)], 'c');
            pred_sigma_graph= plotcov2d(predMu(1), predMu(2), predSigma, 'c', 0, 'y', 0.5, 3);%centerX, centerY, cov, color, filled, fillColor, falpha, nSigma

            plotmarker([mu(1), mu(2)], 'm');
            corr_sigma_graph=plotcov2d(mu(1), mu(2), Sigma, 'm', 0, 'y', 0.5, 3);%centerX, centerY, cov, color, filled, fillColor, falpha, nSigma
            legend([pred_sigma_graph,corr_sigma_graph],{'Pred UKF','Corrected UKF'});
            title('UKF Robot Localization')
        case{'PF'}
            plotSamples(samples);
            title('PF Robot Localization')
        case{'kidnapped'}
            plotSamples(samples);
            plotmarker([mu(1), mu(2)], 'm');
            corr_sigma_graph=plotcov2d(mu(1), mu(2), Sigma, 'm', 0, 'y', 0.5, 3);
    end
    
    %=================================================
    %Evaluate Error
    %=================================================
    
    results(1,t) = t; 
    results(2,t)= x-mu(1); %x_mu error
    results(3,t)= y-mu(2); %y_mu error
    results(4,t)= minimizedAngle(theta-mu(3));%theta_mu error
    results(5,t)= 3*sqrt(Sigma(1,1)); %3 Sigma error in X
    results(6,t)= 3*sqrt(Sigma(2,2)); %3 Sigma error in X
    results(7,t)= 3*sqrt(Sigma(3,3)); %3 Sigma error in X
    
    drawnow;
    if pauseLen == inf
        pause;
    elseif pauseLen > 0
        pause(pauseLen);
    end

    if makeVideo
        F = getframe(gcf);
        switch votype
          case 'avifile'
            vo = addframe(vo, F);
          case 'VideoWriter'
            writeVideo(vo, F);
          otherwise
            error('unrecognized votype');
        end
    end
end

%=================================================
%Part 2: Evaluate Error
%=================================================
figure()
subplot(3,1,1)
plot(results(1,:),results(2,:), 'b',...
    results(1,:), results(5,:),'r',results(1,:), -results(5,:),'r')
title(sprintf('%s: Error in X Direction', method))
xlabel('Time (s)')
ylabel('Distance (m)')
legend('Error in X', '3 Sig. Uncertainty Bound')
subplot(3,1,2)
plot(results(1,:),results(3,:), 'b',...
    results(1,:), results(6,:),'r',results(1,:), -results(6,:),'r')
title('Error in Y Direction')
title(sprintf('%s: Error in Y Direction', method))
legend('Error in Y', '3 Sig. Uncertainty Bound')
xlabel('Time (s)')
ylabel('Distance (m)')
subplot(3,1,3)
plot(results(1,:),results(4,:), 'b',...
    results(1,:), results(7,:),'r',results(1,:), -results(7,:),'r')
xlabel('Time (s)')
ylabel('Angle (rad)')
title(sprintf('%s: Error in Theta', method))
legend('Error', '3 Sig. Uncertainty Bound')

if nargout >= 1
    varargout{1} = data;
end
if nargout >= 2
    varargout{2} = results;
end

if makeVideo
    fprintf('Writing video...');
    switch votype
      case 'avifile'
        vo = close(vo);
      case 'VideoWriter'
        close(vo);
      otherwise
        error('unrecognized votype');
    end
    fprintf('done\n');
end


function s = pfsample(mu, Sigma)

[V,D] = eig(Sigma);

% s=mu+V*sqrt(D)*randn(length(mu),1);
s=mu+V*sqrt(D)*randn(size(mu,1),size(mu,2));

function [ ] = task1(N)
b = 1; 
sample_CDF(N,b);
sample_rejection(N,b);
end

function[] = sample_CDF(N,b)
    sample = [];
    u =rand(N,1);
    
    for i = 1:N
        %return x = F^-1(u)
        if u(i) < .25
            sample(i) = sqrt(0.25*u(i));
        else %if u(i)>=0.25 && u(i)<=1.00
            c = (4*0.25^2)/3 - (8*0.25)/3 + 0.25- u(i);
            sample(i) = (-8/3+sqrt((8/3)^2 + 4*(4/3)*c))/(2*-4/3);
        end
    end
    
    subplot(3,2,1)
    n = zeros(size(u,1),1);
    plot(sample,n, '.k','MarkerSize',1);
    str = sprintf('Sample Distribution N = %d , ndraws = %d',N,N);
    title(str);
    axis([-0.1 1.1 -1 1])
    
    subplot(3,2,3)
    histogram(sample,20);%, 'Normalization', 'probability')
    title('Histogram')
    xlim([-0.1 1.1])
    ylim([0 1200])
    
    x = linspace(0,1,20);
    [emperical,xx] = ksdensity(sample,x);
    subplot(3,2,5)
    plot(xx, emperical,'.-');
    xlim([-0.1 1.1])
    title('Emperical PDF')
    xlabel('CDF Sampling')
end

function[sample] = sample_rejection(N,b, handle)
   sample = [];
   x = rand(N,1);
   y = rand(N,1)*2;
   i = 1;
   k = 1;
   nDraws = 0;
   while k < N+1
        if x(i)<0.25
            fx = 8*x(i);
        else
            fx = 8/3 - 8/3 * x(i);
        end

        if fx >= y(i)
            sample(k) = x(i);
            k = k+1;
        end

        i = i+1;
        if i > N
            i = 1;
            x = rand(N,1);
            y = rand(N,1)*2;
        end
        nDraws = nDraws+1;

    end
    figure = gcf
    subplot(3,2,2)
    n = zeros(1,size(sample,2));
    plot(sample,n, '.k','MarkerSize',1);
    axis([-0.1 1.1 -1 1])
    str = sprintf('Sample Distribution N = %d , ndraws = %d',N,nDraws);
    title(str);
    
    subplot(3,2,4)
    hgt = histogram(sample,20);%, 'Normalization', 'probability')
    title('Histogram')
    xlim([-0.1 1.1])
    ylim([0 1200])
    
    x = linspace(0,1,20);
    [emperical,xx] = ksdensity(sample,x);
    subplot(3,2,6)
    plot(xx, emperical,'.-');
    xlim([-0.1 1.1])
    title('Emperical PDF')
    xlabel('Rejection Sampling')

end
